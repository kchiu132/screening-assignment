public class ClauseSearchController {

    public static Integer i;
    
    public ClauseSearchController(Id id){
        List<Contract_Clause__c> ccList = [SELECT id, name, contract__c, contract__r.name, clause__c, clause__r.name FROM Contract_Clause__c WHERE contract__c=:id];
        i = ccList.size();
    }
    
    @AuraEnabled
    public static List<Contract_Clause__c> fetchCC(Id id){
        List<Contract_Clause__c> ccList = [SELECT id, name, contract__c, contract__r.name, clause__c, clause__r.name FROM Contract_Clause__c WHERE contract__c=:id];
        return ccList;
    }
    
    @AuraEnabled
    public static List<Clause__c> searchAll(){
        return [select id, name, type__c, description__c from clause__c ];
    }
    
    @AuraEnabled
    public static List<Clause__c> searchByName(String searchKey){
        String name = '%' + searchKey + '%';
        return [select id, name, type__c, description__c from clause__c where name like :name LIMIT 50];
    }
    
    @AuraEnabled
    public static void createContractClause(Id contrId, List<Id> clauseIdList){
        ClauseSearchController csc = new ClauseSearchController(contrId);
        List<Contract_Clause__c> ccList = new List<Contract_Clause__c>();
        
        for(Id clId:clauseIdList){
            Contract_Clause__c cc = new Contract_Clause__c(Name='Contract Clause ' + i,
                										   Contract__c=contrId,
                                                           Clause__c=clId
            											  );
            ccList.add(cc);
            i++;
        }
        try{
            if(ccList.size() > 0){
        		insert ccList;
            }
        }catch(DMLException e){
            for(Contract_Clause__c concl:ccList){
            	concl.addError('An error has occurred: ' + e.getMessage());
            }
        }
    }
    
    @AuraEnabled
    public static void deleteContractClause(List<Contract_Clause__c> ccList){
        //ccList will be passed IDs of Contract Clauses
        //add each of those Contract Clauses by ID to a to-delete list
        //List<Contract_Clause__c> toDelete =  [SELECT Id FROM Contract_Clause__c WHERE Id IN :ccList];
        //try to delete
        try{
        	delete ccList;
        }catch(Exception e){
            for(Contract_Clause__c concl:ccList){
                concl.addError('An error has occurred. ' + e.getMessage());
            }
        }
    }
}