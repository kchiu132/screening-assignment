public class CustomContractController {

    @AuraEnabled
    public static List<Contract_Clause__c> fetchContractClauses(Id id){
        List<Contract_Clause__c> ccList = [SELECT id, name, contract__c, contract__r.name, clause__c, clause__r.name FROM Contract_Clause__c WHERE contract__c=:id];
        return ccList;
    }
    
}