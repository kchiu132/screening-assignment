@isTest
public class CodeTesting {
    
    @isTest
    public static void constructorTest(){
        Account act = new Account(Name='Billy Bob');

        Contract con = new Contract(Name='Test Contract',
                                    AccountId=act.Id,
                                    StartDate=Date.newInstance(2018, 8, 8),
                                    ContractTerm=12
                          );
        
        Clause__c cl = new Clause__c(Name='Test Clause',
                                     Type__c='Financial',
                                     Description__c='described'
                                    );
        
        Contract_Clause__c cc = new Contract_Clause__c(Name='Test CC',
                                                       Contract__c=con.Id,
                                                       Clause__c=cl.Id
                                                      );
        
        test.starttest();
        ClauseSearchController csc = new ClauseSearchController(con.Id);
        test.stoptest();
        
        system.assertequals(ClauseSearchController.i, 1);
    }

    @isTest
    public static void fetchTest(){
        
        Account act = new Account(Name='Billy Bob');

        Contract con = new Contract(Name='Test Contract',
                                    AccountId=act.Id,
                                    StartDate=Date.newInstance(2018, 8, 8),
                                    ContractTerm=12
                          );
        
        Clause__c cl = new Clause__c(Name='Test Clause',
                                     Type__c='Financial',
                                     Description__c='described'
                                    );
        
        Clause__c cl2 = new Clause__c(Name='Santa Clause',
                                     Type__c='Other',
                                     Description__c='ho ho ho!'
                                    );
        
        Contract_Clause__c cc = new Contract_Clause__c(Name='Test CC',
                                                       Contract__c=con.Id,
                                                       Clause__c=cl.Id
                                                      );
        
        //testing fetchCC
        test.startTest();
        con.contract_clauses__r.add(cc); //add contract clause to contract
        ClauseSearchController.fetchCC(con.Id);
        test.stopTest();
        system.assertequals(cc.Id, con.contract_clauses__r[1].Id);
    }
    
    @isTest
    public static void searchAllTest(){
        Account act = new Account(Name='Billy Bob');

        Contract con = new Contract(Name='Test Contract',
                                    AccountId=act.Id,
                                    StartDate=Date.newInstance(2018, 8, 8),
                                    ContractTerm=12
                          );
        
        Clause__c cl = new Clause__c(Name='Test Clause',
                                     Type__c='Financial',
                                     Description__c='described'
                                    );
        
        Clause__c cl2 = new Clause__c(Name='Santa Clause',
                                     Type__c='Other',
                                     Description__c='ho ho ho!'
                                    );
        
        Contract_Clause__c cc = new Contract_Clause__c(Name='Test CC',
                                                       Contract__c=con.Id,
                                                       Clause__c=cl.Id
                                                      );
        //searchAll
        test.starttest();
        insert cl;
        List<Clause__c> testList1 = ClauseSearchController.searchAll();
        test.stoptest();
        system.assertequals(1, testList1.size());
    }
    
    @isTest
    public static void byNameTest(){
        Account act = new Account(Name='Billy Bob');

        Contract con = new Contract(Name='Test Contract',
                                    AccountId=act.Id,
                                    StartDate=Date.newInstance(2018, 8, 8),
                                    ContractTerm=12
                          );
        
        Clause__c cl = new Clause__c(Name='Test Clause',
                                     Type__c='Financial',
                                     Description__c='described'
                                    );
        
        Clause__c cl2 = new Clause__c(Name='Santa Clause',
                                     Type__c='Other',
                                     Description__c='ho ho ho!'
                                    );
        
        Contract_Clause__c cc = new Contract_Clause__c(Name='Test CC',
                                                       Contract__c=con.Id,
                                                       Clause__c=cl.Id
                                                      );
        //searchByName
        test.starttest();
        insert cl;
        List<Clause__c> testList2 = ClauseSearchController.searchByName('Test');
        test.stoptest();
        system.assertequals('Test Clause', testList2[0].Name);
    }
    
    @isTest
    public static void createTest(){
        Account act = new Account(Name='Billy Bob');

        Contract con = new Contract(Name='Test Contract',
                                    AccountId=act.Id,
                                    StartDate=Date.newInstance(2018, 8, 8),
                                    ContractTerm=12
                          );
        
        Clause__c cl = new Clause__c(Name='Test Clause',
                                     Type__c='Financial',
                                     Description__c='described'
                                    );
        
        Clause__c cl2 = new Clause__c(Name='Santa Clause',
                                     Type__c='Other',
                                     Description__c='ho ho ho!'
                                    );
        
        Contract_Clause__c cc = new Contract_Clause__c(Name='Test CC',
                                                       Contract__c=con.Id,
                                                       Clause__c=cl.Id
                                                      );
        //createContractClause
        test.starttest();
        List<Id> clIds = new List<Id>();
        clIds.add(cl2.Id);
        ClauseSearchController.createContractClause(con.Id, clIds);
        test.stoptest();
        system.assertequals('Contract Clause 0', ClauseSearchController.searchByName('Contract Clause 0')[0].Name);
    }
    
    @isTest
    public static void deleteTest(){
        Account act = new Account(Name='Billy Bob');

        Contract con = new Contract(Name='Test Contract',
                                    AccountId=act.Id,
                                    StartDate=Date.newInstance(2018, 8, 8),
                                    ContractTerm=12
                          );
        Contract con2 = new Contract(Name='Fake Contract',
                                    AccountId=act.Id,
                                    StartDate=Date.newInstance(1969, 7, 14),
                                    ContractTerm=12
                                   );
        
        Clause__c cl = new Clause__c(Name='Test Clause',
                                     Type__c='Financial',
                                     Description__c='described'
                                    );
        
        Clause__c cl2 = new Clause__c(Name='Santa Clause',
                                     Type__c='Other',
                                     Description__c='ho ho ho!'
                                    );
        
        Contract_Clause__c cc = new Contract_Clause__c(Name='Test CC',
                                                       Contract__c=con.Id,
                                                       Clause__c=cl.Id
                                                      );
        Contract_Clause__c cc2 = new Contract_Clause__c(Name='Fake CC',
                                                        Contract__c=con2.Id,
                                                        Clause__c=cl2.Id
                                                       );
        
        //deleteContractClause
        //success
        test.starttest();
        List<Contract_Clause__c> idList = new List<Contract_Clause__c>();
        idList.add(cc);
        ClauseSearchController.deleteContractClause(idList);
        List<Contract_Clause__c> testList = ClauseSearchController.fetchCC(con.Id);
        test.stoptest();
        system.assertequals(0, testList.size());
        //fail
        test.starttest();
        List<Contract_Clause__c> idList2 = new List<Contract_Clause__c>();
        idList2.add(cc2);
        try{
        	ClauseSearchController.deleteContractClause(idList2);
        }catch(Exception e){
            //expected error should contain the phrase we input in the other class. if so, return true. if not, return false.
            boolean expected = e.getmessage().contains('An error has occurred.') ? true : false;
            //our expected error should have fired, so assert that the return value is true
            system.assertequals(expected, true);
        }
        test.stoptest();
        
    }
    
    @isTest
    public static void custContrContTest(){
        Account act = new Account(Name='Billy Bob');
        
        Contract con = new Contract(Name='Test Contract',
                                    AccountId=act.Id,
                                    StartDate=Date.newInstance(2018, 8, 8),
                                    ContractTerm=12
                          );
        
        Clause__c cl = new Clause__c(Name='Test Clause',
                                     Type__c='Financial',
                                     Description__c='described'
                                    );
        Clause__c cl2 = new Clause__c(Name='Santa Clause',
                                     Type__c='Other',
                                     Description__c='ho ho ho!'
                                    );
        
        Contract_Clause__c cc = new Contract_Clause__c(Name='Test CC',
                                                       Contract__c=con.Id,
                                                       Clause__c=cl.Id
                                                      );
        Contract_Clause__c cc2 = new Contract_Clause__c(Name='Test CC2',
                                                       Contract__c=con.Id,
                                                       Clause__c=cl2.Id
                                                      );
        test.starttest();
        List<Contract_Clause__c> testList = CustomContractController.fetchContractClauses(con.Id);
        test.stoptest();
        system.assertequals(2, testList.size());
        
        
    }
    
}