({
	doInit : function(component, event, helper) {
		var action = component.get("c.fetchContractClauses");
        action.setParams({"id" : component.get("v.recordId")});
        //console.log("Record id: " + component.get("v.recordId"))
        
        action.setCallback(this, function(a) {
            if(a.getState() === "SUCCESS"){
                //console.log("I'm here!");
            	component.set("v.contractclauses", a.getReturnValue())
                //console.log("cclist: " + component.get("v.contractclauses"));
        	}
        });
        
        //console.log("contract clause list: " + component.get("v.contractclauses"))
        
        $A.enqueueAction(action);
	},
    closeModal:function(component,event,helper){    
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    openmodal: function(component,event,helper) {
        var cmpTarget = component.find('Modalbox');
        var cmpBack = component.find('Modalbackdrop');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    },
    closeModal2:function(component,event,helper){    
        var cmpTarget = component.find('Modalbox2');
        var cmpBack = component.find('Modalbackdrop2');
        $A.util.removeClass(cmpBack,'slds-backdrop--open');
        $A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    },
    openmodal2: function(component,event,helper) {
        var cmpTarget = component.find('Modalbox2');
        var cmpBack = component.find('Modalbackdrop2');
        $A.util.addClass(cmpTarget, 'slds-fade-in-open');
        $A.util.addClass(cmpBack, 'slds-backdrop--open'); 
    }
})