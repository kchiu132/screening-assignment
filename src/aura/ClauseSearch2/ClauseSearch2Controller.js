({
 
    doInit: function(component, event, helper) {
        helper.getRecords(component, event);
        console.log(component.get("v.recId"));
    },
 
    addSelected2: function(component, event, helper) {
        var tempIDs = [];
 
        var getAllId = component.find("checkBox");
 
        for (var i = 0; i < getAllId.length; i++) {
            
       if (getAllId[i].get("v.value") == true) {
                tempIDs.push(getAllId[i].get("v.text"));
            }
        }
   
        helper.addSelectedHelper2(component, event, tempIDs);
    },
})