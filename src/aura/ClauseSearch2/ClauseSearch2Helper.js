({
    getRecords: function(component, event) {
        var action = component.get('c.fetchCC');
        action.setParams({
            "id": component.get("v.recId")
        });
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === 'SUCCESS') {
                component.set('v.contractclauses', actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
 
    addSelectedHelper2: function(component, event, recordsIds) {
        var action = component.get('c.deleteContractClause');
        action.setParams({
            "ccList": recordsIds
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // display SUCCESS message
                 var toastEvent = $A.get("e.force:showToast");
          			toastEvent.setParams({
        				"title": "Success!",
        				"message": "Succesfully removed record(s)."
    				});
                    toastEvent.fire();
              
                // call init function again [clear selected checkboxes]
                this.getRecords(component,event);
                  
            }
        });
        var sObjectEvent = $A.get("e.force:navigateToSObject");
                    sObjectEvent.setParams({
                        "recordId": component.get("v.recId"),
                        "slideDevName": 'related'
                    })
                    sObjectEvent.fire();
        $A.enqueueAction(action);
    }
})