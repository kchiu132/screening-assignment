({
 
    doInit: function(component, event, helper) {
        helper.getRecords(component, event);
        console.log(component.get("v.recId"));
    },
 
    addSelected: function(component, event, helper) {
        var tempIDs = [];
 
        var getAllId = component.find("checkBox");
 
        for (var i = 0; i < getAllId.length; i++) {
            
       if (getAllId[i].get("v.value") == true) {
                tempIDs.push(getAllId[i].get("v.text"));
            }
        }
   
        helper.addSelectedHelper(component, event, tempIDs);
    },
    searchKeyChange: function(component, event, helper) {
       var myEvent = $A.get("e.c:searchKeyEvent");
   	   console.log('fired keyboard event with the following value'+event.target.value);
         
       myEvent.setParams({"searchkey1": event.target.value});
       myEvent.fire();
   },
    searchKeyChange2 : function(component, event){
        var searchKey = event.getParam("searchkey1");
        var action1 = component.get("c.searchByName");
        action1.setParams({
            "searchKey" : searchKey
        });
        action1.setCallback(this, function(a){
        	component.set("v.ClauseList",a.getReturnValue());                                   
		});
        $A.enqueueAction(action1);
    }
})