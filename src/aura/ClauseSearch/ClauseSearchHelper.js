({
    getRecords: function(component, event) {
        var action = component.get('c.searchAll');
        action.setCallback(this, function(actionResult) {
            var state = actionResult.getState();
            if (state === 'SUCCESS') {
                component.set('v.ClauseList', actionResult.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
 
    addSelectedHelper: function(component, event, recordsIds) {
        var action = component.get('c.createContractClause');
        action.setParams({
            "contrId": component.get("v.recId"), 
            "clauseIdList": recordsIds
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // display SUCCESS message
                 var toastEvent = $A.get("e.force:showToast");
          			toastEvent.setParams({
        				"title": "Success!",
        				"message": "Succesfully added record(s)."
    				});
                    toastEvent.fire();
              
                // call init function again [clear selected checkboxes]
                this.getRecords(component,event);
                  
            }
        });
        var sObjectEvent = $A.get("e.force:navigateToSObject");
                    sObjectEvent.setParams({
                        "recordId": component.get("v.recId"),
                        "slideDevName": 'related'
                    })
                    sObjectEvent.fire();
        $A.enqueueAction(action);
    }
})